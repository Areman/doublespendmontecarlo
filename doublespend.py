"""This project is a class assignment and is not supposed to be published.
A Monte Carlo simulation of double spend attack is implemented in this
project where the attacker mining power is assumed to be q percent
of the total mining power and the merchant is handing the good
over to the attacker after having z blocks added to the chain
after the block containing the fund transfer transaction.
The simulation is run for q values between 4 an 40 percent
(inclusive increments of 2%) and z values between 1 and 10
(inclusive increments of 1).
"""
from calendar import datetime
import random

def main():
    """The main function.
    """
    _giveup_threshold = 35
    output_file_name = 'results'
    date_now = datetime.datetime.now()
    date_now_string = str(date_now)
    date_now_string = date_now_string.replace(' ', '')
    date_now_string = date_now_string.replace(':', '')
    date_now_string = date_now_string.replace('.', '')
    date_now_string = date_now_string.replace('-', '')
    output_file_name += str('.' + date_now_string + '.csv')
    #output_file_name += '.csv'
    output_file = open(output_file_name, "w")
    output_file.write('q' + '\t' + 'z' + '\t' + 'test' + '\n')
    while True:
        round_results = {}
        for z_index in range(1, 11, 1):
            for q_index in range(4, 42, 2):
                #output_file.write(str(q_index) + '\t' + str(z_index) +'\n')
                round_results[(z_index, q_index)] =\
                test_attacker_success_simple_version(z_index, q_index, _giveup_threshold)
        for z_index in range(1, 11, 1):
            for q_index in range(4, 42, 2):
                output_file.write(str('%0.3f' % (float(q_index)/100)) + '\t' + str(z_index) + '\t' \
                + str(round_results[(z_index, q_index)]) +'\n')
    #print output_file_name
def test_attacker_success_simple_version(embargo_length, attackers_power, giveup_threshold):
    """Runs a test for embargo period of z and attacker's power of q.
    It's a single test which should be run millions of times by the
    main function
    """
    attackers_blocks = 0
    honests_blocks = 0
    while True:
        random_int = random.randrange(1, 101, 1)
        if random_int <= attackers_power:
            attackers_blocks += 1
        else:
            honests_blocks += 1
        if giveup_threshold <= honests_blocks - attackers_blocks:
            return 0
        elif attackers_blocks > embargo_length:
            if attackers_blocks > honests_blocks:
                return 1
def test_attacker_success_levine_version(embargo_length, attackers_power, giveup_threshold):
    """Runs a test for embargo period of z and attacker's power of q.
    It's a single test which should be run millions of times by the
    main function
    """
    _hardness_index = 100
    attackers_blocks = 0
    honests_blocks = 0
    while True:
        generate_block_decision = random.randrange(1, _hardness_index, 1)
        if generate_block_decision == 1:
            random_int = random.randrange(1, 101, 1)
            if random_int <= attackers_power:
                attackers_blocks += 1
        generate_block_decision = random.randrange(1, _hardness_index, 1)
        if generate_block_decision == 1:
            random_int = random.randrange(1, 101, 1)
            if random_int <= 100 - attackers_power:
                honests_blocks += 1                
        if giveup_threshold <= honests_blocks - attackers_blocks:
            return 0
        elif attackers_blocks > embargo_length:
            if attackers_blocks > honests_blocks:
                return 1    
if __name__ == '__main__':
    main()
