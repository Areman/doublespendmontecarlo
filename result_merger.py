from os import listdir
from os.path import isfile, join

def main():
    _minz_threshold = 0.01
    only_files = [f for f in listdir('./') if isfile(join('./', f))]
    number_of_tests = 0
    number_of_sucesses = {}
    this_round_test = {}
    for z_index in range(1, 11, 1):
        for q_index in range(4, 42, 2):
            number_of_sucesses[(z_index, q_index)] = 0
    this_round_test = number_of_sucesses
    for i in range(len(only_files)):
    ###Iterating over all the files in the current directory
        current_file_name = only_files[i]
        split = current_file_name.split('.', 1)
        if split[0] != 'results':
            continue
        else:
        ###Only process result files
            current_file = open(current_file_name, 'r')
            current_line = current_file.readline()
            flag = 0
            while current_line:
                for z_index in range(1, 11, 1):
                    for q_index in range(4, 42, 2):
                        current_line = current_file.readline()
                        if current_line == None:
                            flag = 1
                            break
                        configuration = current_line.split('\t', 2)
                        if len(configuration) > 2:
                            result = configuration[2].split('\n', 1)
                            if len(result) < 2:
                                flag = 1
                                break
                        else:
                            flag = 1
                            break
#                        print configuration[2], current_line,
                        if int(result[0]) == 1:
                           #number_of_sucesses[(z_index, q_index)] += 1
                           this_round_test[(z_index, q_index)] = \
                           number_of_sucesses[(z_index, q_index)] + 1
                    if flag == 1:
                        break
                if flag == 1:
                    break
                else:
                    pass
                    number_of_sucesses = this_round_test
                    #print number_of_sucesses[1, 38]
                number_of_tests += 1
        output_file = open("agg.csv", 'w')
        minz_output_file = open("minz.csv", 'w')
        minz_output_file.write('q' + '\t' + 'minz' + '\n')
        output_file.write('q' + '\t' + 'z' + '\t' + 'successes' + '\t' + 'tests' + '\t' + 'prob' + '\n')
        for q_index in range(4, 42, 2):
            minz = 'N/A'
            for z_index in range(10, 0, -1):
                if float(number_of_sucesses[(z_index, q_index)])/number_of_tests < _minz_threshold:
                    minz = z_index
                output_file.write(str('%0.3f' % (float(q_index)/100)) + '\t' + str(z_index) + '\t' + \
                str(number_of_sucesses[(z_index, q_index)]) + '\t' + \
                str(number_of_tests) + '\t' + \
                str(float(number_of_sucesses[(z_index, q_index)])/number_of_tests) +\
                '\n')
            minz_output_file.write(str(q_index) + '\t' + str(minz) + '\n')
        output_file.close()
        minz_output_file.close()
        
if __name__ == '__main__':
    main()
